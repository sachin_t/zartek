import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../Model/get_restaurant_list_model.dart';
import '../Service/api_service.dart';

class DashboardBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => DashboardController());
  }
}

class DashboardController extends GetxController with GetTickerProviderStateMixin {
  final RxList<Tab> tabs = <Tab>[].obs;

  late TabController tabController;
  RxInt cartCount = 0.obs;
  static DashboardController get to => Get.put(DashboardController());

  RxBool isLoading = false.obs;
  RxList<GetRestaurantListModel> list = <GetRestaurantListModel>[].obs;

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();

  void openDrawer() {
    scaffoldKey.currentState?.openDrawer();
  }

  void closeDrawer() {
    scaffoldKey.currentState?.openEndDrawer();
  }

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    tabController = TabController(vsync: this, length: tabs.length);
    getRestaurantLists();
  }

  void getRestaurantLists() async {
    isLoading.value = true;
    try {
    var data = await ApiService.getRestaurantLists();

    if (data != []) {
      data[0].tableMenuList?.forEach((element) {
        tabs.add(Tab(text: element.menuCategory));
      });
      print('rest list= ${data[0].restaurantName}');
      list.value = data;
      await Future.delayed(const Duration(seconds: 2), () {
        tabController.dispose();
        tabController = TabController(
            vsync: this,
            length: tabs.length,
        );
      });
    }} catch (e) {
      Get.snackbar('Error', 'Something went wrong');
    } finally {
      update();
      isLoading.value = false;
    }
  }

  void placeOrder() {

  }

}

class Cart {
  RxInt cartCount = 0.obs;

  Cart(this.cartCount);

}
