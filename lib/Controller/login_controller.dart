import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:zartek_e_commerce/PageRoute/page_route.dart';
import 'package:zartek_e_commerce/main.dart';


class LoginBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => LoginController());
  }
}

class LoginController extends GetxController {

  static LoginController get to => Get.put(LoginController());
  final formKey = GlobalKey<FormState>();

  TextEditingController userNameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController mobileController = TextEditingController();
  TextEditingController otpController = TextEditingController();
  final FirebaseAuth auth = FirebaseAuth.instance;
  RxBool otpSent = false.obs;
  String phoneNumber = '';
  String verifyId = '';

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
  }

  Future<void> verifyPhoneNumber(String phoneNumber) async {

    verificationCompleted(PhoneAuthCredential credential) async {
      UserCredential userCredential = await auth.signInWithCredential(credential);
      print('User signed in: ${userCredential.user}');
    }

    verificationFailed(FirebaseAuthException e) {
      print('Phone number verification failed. Code: ${e.message}');
    }

    codeSent(String verificationId, int? resendToken) async {
      print('Code sent to ${verificationId}');
      verifyId = verificationId;
      otpSent.value = true;
    }

    codeAutoRetrievalTimeout(String verificationId) {
      print('Auto retrieval timeout');
      verifyId = verificationId;
    }

    await auth.verifyPhoneNumber(
      phoneNumber: phoneNumber,
      verificationCompleted: verificationCompleted,
      verificationFailed: verificationFailed,
      codeSent: codeSent,
      codeAutoRetrievalTimeout: codeAutoRetrievalTimeout,
    );
  }

  Future<bool> verifyOtp (smsCode) async {
    var credentials = await auth.signInWithCredential(
        PhoneAuthProvider.credential(verificationId: verifyId, smsCode: smsCode));
    if (credentials.user != null) {
      Get.toNamed(AppRoutes.dashBoard);
    }
    return credentials.user != null? true : false;
  }

  Future<UserCredential> signInWithGoogle() async {
    // Trigger the authentication flow
    final GoogleSignInAccount? googleUser = await GoogleSignIn().signIn();

    // Obtain the auth details from the request
    final GoogleSignInAuthentication? googleAuth = await googleUser?.authentication;

    // Create a new credential
    final credential = GoogleAuthProvider.credential(
      accessToken: googleAuth?.accessToken,
      idToken: googleAuth?.idToken,
    );

    // Once signed in, return the UserCredential
    return await FirebaseAuth.instance.signInWithCredential(credential);
  }

}
