import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:zartek_e_commerce/Controller/dashboard_controller.dart';

import '../Model/get_restaurant_list_model.dart';

class OrderSummaryBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => OrderSummaryController());
  }
}

class OrderSummaryController extends GetxController {

  List<TableMenuList>? dataList;
  RxInt cartCount = 0.obs;
  RxInt dishCount = 0.obs;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    dataList = Get.arguments['dataList'];
    cartCount.value = Get.arguments['cartCount'];

    if (dataList![0].categoryDishes![0].cartCount != 0) {
      dishCount.value++;
    }
  }

  void placeOrder() {
    Get.defaultDialog(
      title: "Thank You",
      middleText: "Order Successfully Placed",
      backgroundColor: Colors.white,
      titleStyle: GoogleFonts.openSans(
          fontWeight: FontWeight.w600,
          color: Colors.teal,
          fontSize: 20.sp),
      middleTextStyle: GoogleFonts.openSans(color: Colors.teal,fontSize: 16.sp),
      radius: 10.r,
    );
    Future.delayed(const Duration(seconds: 1), () {
      DashboardController.to.getRestaurantLists();
      DashboardController.to.cartCount.value = 0;
      Get.back();
      Get.back();
    });
  }

}