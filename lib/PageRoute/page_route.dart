import 'package:get/get.dart';
import 'package:zartek_e_commerce/Controller/order_summary_controller.dart';
import 'package:zartek_e_commerce/View/dashboard_page.dart';
import 'package:zartek_e_commerce/View/order_summary_page.dart';

import '../Controller/dashboard_controller.dart';
import '../Controller/login_controller.dart';
import '../View/login_view.dart';

class AppRoutes {
  static const login = '/login';
  static const dashBoard = '/dashBoard';
  static const orderSummary = '/orderSummary';
}

class AppPages {
  static final List<GetPage> pages = [
    GetPage(
      name: AppRoutes.login,
      page: () => const LoginView(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: AppRoutes.dashBoard,
      page: () => DashBoardPage(),
      binding: DashboardBinding(),
    ),
    GetPage(
      name: AppRoutes.orderSummary,
      page: () => OrderSummaryPage(),
      binding: OrderSummaryBinding(),
    ),
  ];
}
