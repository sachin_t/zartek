import 'dart:convert';
import 'dart:developer';

import 'package:get/get.dart';
import 'package:http/http.dart' as http;

import 'package:zartek_e_commerce/Model/get_restaurant_list_model.dart';

const String r3UrlGetCustomQData = "/MasterType/GetCustomQData/";


class ApiService {

    static Future<List<GetRestaurantListModel>> getRestaurantLists() async {

    Map<String, String> headers = {
      "Accept": "application/json",
    };
    http.Response response = await http.get(
        Uri.parse('https://run.mocky.io/v3/4d116e3e-808c-43ab-93ed-6c70540d4e18'), headers: headers);

    if (response.statusCode == 200) {
      log(response.body);
      return getRestaurantListModelFromJson(response.body);
    } else {
      Get.snackbar('Error!', "Something went wrong");
      return getRestaurantListModelFromJson(response.body);
    }
  }

}