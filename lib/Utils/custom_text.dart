import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';


class CustomText extends StatelessWidget {
  String? text;
  double? fontSize;
  double? height;
  int? maxLines;
  FontWeight? fontWeight;
  Color? color;
  TextAlign? alignment;
  CustomText({Key? key, this.color, this.text, this.height,
    this.fontWeight, this.fontSize, this.maxLines, this.alignment}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(text??'',
      textAlign: alignment ?? TextAlign.center,
      maxLines: maxLines,
      style: GoogleFonts.openSans(
        fontSize: fontSize ?? 14.sp,
        color: color ?? Colors.black,
        height: height ?? 1.2.h,
        fontWeight: fontWeight ?? FontWeight.w400
      ),
    );
  }
}
