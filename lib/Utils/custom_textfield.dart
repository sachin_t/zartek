import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:zartek_e_commerce/Utils/custom_text.dart';


class CustomTextField extends StatefulWidget {
  double? height;
  String? label;
  bool isMobile;
  TextEditingController controller;
  CustomTextField({super.key, this.height, this.isMobile = false, this.label, required this.controller});

  @override
  State<CustomTextField> createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  bool passwordVisible = false;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: widget.controller,
      validator: (value) {
        if (value!.isEmpty) {
          return '';
        }
        return null;
      },

      decoration: InputDecoration(
          enabledBorder: border,
        alignLabelWithHint: false,
        prefix: widget.isMobile? CustomText(
          text: '+91 ',
          fontSize: 15.sp,
          height: 1.6.h,
          fontWeight: FontWeight.w600,
        ) : const SizedBox(),
        hintText: widget.label,
        labelText: widget.label,
        hintStyle: GoogleFonts.openSans(
          color: const Color(0x8A000000),
          fontSize: 14.sp
        ),
        errorStyle: const TextStyle(
          height: 0
        ),
        focusedBorder: border,
        errorBorder: errorBorder,
        focusedErrorBorder: errorBorder,
        contentPadding: EdgeInsets.symmetric(vertical: 5.h, horizontal: 10.w),
      ),
    );
  }
}

OutlineInputBorder get border => OutlineInputBorder(
    borderSide: const BorderSide(
        color: Colors.blueAccent
    ),
    borderRadius: BorderRadius.all(
        Radius.circular(4.r))
);

OutlineInputBorder get errorBorder => OutlineInputBorder(
    borderSide: const BorderSide(
        color: Colors.redAccent
    ),
    borderRadius: BorderRadius.all(
        Radius.circular(4.r))
);