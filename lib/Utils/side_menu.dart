import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../PageRoute/page_route.dart';
import 'custom_text.dart';

class MenuDrawerContent extends StatelessWidget {

  final user = FirebaseAuth.instance.currentUser;
  String name = '';
  String uid = '';

  @override
  Widget build(BuildContext context) {

    final user = this.user;

    if (user != null) {
       name = user.displayName??'--';
       uid = user.uid;
    }

    return Drawer(
      width: 321.w,
      backgroundColor: Colors.purpleAccent,
      child: ListView(
        children: [
          Container(
            height: 110.h,
            width: 321.w,
            color: Colors.purple,
            child: Stack(
                alignment: AlignmentDirectional.topStart,
              children: [
                Positioned(
                  top: -67.h,
                    left: -55.w,
                    child: Container(
                      width: 150.w,
                      height: 162.h,
                      decoration: const BoxDecoration(
                          color: Colors.purple,
                          shape: BoxShape.circle,
                      ),
                    )),
                Positioned(
                 top: 10.h,
                    left: 10.h,
                    child: Row(
                      children: [
                        Container(
                          width: 50.w,
                          height: 50.h,
                          decoration: const BoxDecoration(
                              color: Colors.white,
                              shape: BoxShape.circle,
                          ),
                          child: Icon(
                              Icons.account_circle,
                              size: 45.h,
                              color: Colors.purple),
                        ),
                        SizedBox(width: 10.w),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            CustomText(
                              text: name,
                              fontSize: 16.sp,
                              color: Colors.white,
                              fontWeight: FontWeight.w600,
                            ),
                            SizedBox(height: 10.h),
                            SizedBox(
                              width: 150.w,
                              child: CustomText(
                                text: uid,
                                fontSize: 16.sp,
                                alignment: TextAlign.left,
                                color: Colors.white,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            SizedBox(height: 10.h),
                          ],
                        )
                      ],
                    )),
                ]
            ),
          ),
          InkWell(
            onTap: () async => signOut(),
            child: Container(
              height: 30.h,
              width: 92.w,
              decoration: BoxDecoration(
                  color: Colors.purple,
                  borderRadius: BorderRadius.all(Radius.circular(4.r))
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(width: 50.w),
                  Icon(Icons.logout, color: Colors.white, size: 20.w),
                  CustomText(
                    text: 'Logout',
                    fontSize: 16.sp,
                    color: Colors.white,
                    fontWeight: FontWeight.w600,
                  ),
                ],
              ),
            ),
          ),
          ],
      ),
    );
  }

  Future<void> signOut() async {
    await FirebaseAuth.instance.signOut();
    Get.offAllNamed(AppRoutes.login);
  }

}
