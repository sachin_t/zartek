import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:zartek_e_commerce/Controller/dashboard_controller.dart';
import 'package:zartek_e_commerce/PageRoute/page_route.dart';
import 'package:zartek_e_commerce/Utils/custom_text.dart';

import '../Model/get_restaurant_list_model.dart';
import '../Utils/side_menu.dart';

class DashBoardPage extends StatelessWidget {
  DashBoardPage({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<DashboardController>(
        builder: (c) {
          return DefaultTabController(
            length: c.tabs.length,
            child: Scaffold(
              key: c.scaffoldKey,
              appBar: AppBar(
                bottom: TabBar(
                  controller: c.tabController,
                  tabs: c.tabs,
                ),
                actions: [
                  GestureDetector(
                    onTap: () => Get.toNamed(AppRoutes.orderSummary,
                    arguments: {
                      'dataList' : c.list[0].tableMenuList,
                      'cartCount' : c.cartCount.value
                    }),
                    child: Stack(
                      clipBehavior: Clip.none,
                      children: [
                        Icon(
                          Icons.shopping_cart_outlined,
                          size: 25.h,
                        ),
                        Positioned(
                          left: -3.w,
                          top: -3.h,
                          child: Container(
                            decoration: const BoxDecoration(
                                color: Colors.white,
                                shape: BoxShape.circle
                            ),
                            child: GetX<DashboardController>(
                                builder: (c) {
                                  return CustomText(
                                    text: c.cartCount.value.toString(),
                                    color: Colors.redAccent,
                                    fontWeight: FontWeight.w600,
                                  );
                                }
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
                leading: GestureDetector(
                    onTap: () => c.openDrawer(),
                    child: const Icon(Icons.menu)),
              ),
              drawer: MenuDrawerContent(),
              body: GetX<DashboardController>(
                  builder: (c) {
                    return c.isLoading.value?
                    const Center(
                        child: CircularProgressIndicator(color: Colors.green)):
                    TabBarView(
                    controller: c.tabController,
                    children: c.tabs.map((Tab tab) {
                      List<TableMenuList>? items = c.list[0].tableMenuList;
                      return ListView.builder(
                                itemCount: items?[0].categoryDishes?.length,
                                itemBuilder: (context, index) =>
                                    DishList(index: index, items: items));
                    }).toList(),
                  );
                }
              ),
            ),
          );
        }
    );
  }
}

class DishList extends StatelessWidget {
  DishList({super.key, required this.items, required this.index});

  List<TableMenuList>? items;
  int index;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      padding: EdgeInsets.symmetric(
          horizontal: 10.w,
          vertical: 14.h
      ),
      decoration: const BoxDecoration(
          color: Colors.white,
          border: Border(
              bottom: BorderSide(color: Colors.grey),
              top: BorderSide(color: Colors.grey))
      ),
      child: Row(
        children: [
          Column(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: Colors.green
                          )
                      ),
                      child: const Icon(Icons.circle, color: Colors.green)),
                  SizedBox(width: 10.w),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        width: 220.w,
                        child: CustomText(
                          text: items?[0].categoryDishes?[index].dishName,
                          fontWeight: FontWeight.w600,
                          alignment: TextAlign.start,
                          maxLines: 1,
                          fontSize: 16.sp,
                        ),
                      ),
                      SizedBox(height: 10.w),
                      SizedBox(
                        width: Get.width-160.w,
                        child: Row(
                          children: [
                            Flexible(
                              flex: 1,
                              child: CustomText(
                                text: 'INR ${items?[0].categoryDishes?[index].dishPrice}',
                                fontWeight: FontWeight.w600,
                                fontSize: 15.sp,
                              ),
                            ),
                            const Spacer(),
                            Flexible(
                              flex: 2,
                              child: CustomText(
                                text: '${items?[0].categoryDishes?[index].dishCalories} Calories',
                                fontWeight: FontWeight.w600,
                                fontSize: 15.sp,
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 10.w),
                      SizedBox(
                        width: 220.w,
                        child: CustomText(
                          text: items?[0].categoryDishes?[index].dishDescription,
                          fontWeight: FontWeight.w400,
                          color: Colors.grey,
                          alignment: TextAlign.left,
                          fontSize: 15.sp,
                        ),
                      ),
                      SizedBox(height: 10.w),
                      Container(
                        width: 100.w,
                        height: 30.h,
                        decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius: BorderRadius.all(Radius.circular(10.r))
                        ),
                        child: CartCountBox(index: index, dataList: items?[0].categoryDishes),
                      ),
                      SizedBox(height: 5.h),
                      items![0].categoryDishes![index].addonCat!.isNotEmpty?
                      CustomText(
                          text: 'Customization Available',
                          color: Colors.redAccent,
                          fontSize: 16.sp,
                          fontWeight: FontWeight.w500):const SizedBox(),
                    ],
                  )
                ],
              )
            ],
          ),
          Container(
            height: 88.h,
            width: 88.w,
            margin: EdgeInsets.symmetric(horizontal: 5.w),
            child: ClipRRect(
              borderRadius: BorderRadius.all(
                  Radius.circular(5.r)
              ),
              child: CachedNetworkImage(
                imageUrl: items?[0].categoryDishes?[index].dishImage??'',
                placeholder: (context, url) => const Center(child: CircularProgressIndicator()),
                fit: BoxFit.fill,
                errorWidget: (context, url, error) => const Icon(Icons.error),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class CartCountBox extends StatelessWidget {
  CartCountBox({super.key, required this.index, required this.dataList});

  int index;
  List<CategoryDish>? dataList;

  @override
  Widget build(BuildContext context) {
    return GetBuilder <DashboardController>(
        builder: (c) {
          return Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              GestureDetector(
                onTap: () {
                if (dataList![index].cartCount > 0) {
                  c.cartCount.value--;
                  dataList?[index].cartCount --;
                  c.update();
                }},
                child: CustomText(
                    text: '-',
                    color: Colors.white,
                    fontSize: 17.sp,
                    fontWeight: FontWeight.w700),
              ),
              CustomText(
                  text: dataList?[index].cartCount.toString(),
                  color: Colors.white,
                  fontSize: 17.sp,
                  fontWeight: FontWeight.w700),
              GestureDetector(
                onTap: () {
                  c.cartCount.value++;
                  dataList?[index].cartCount++;
                  c.update();
                },
                child: CustomText(
                    text: '+',
                    color: Colors.white,
                    fontSize: 17.sp,
                    fontWeight: FontWeight.w700),
              ),
            ],
          );
        }
    );
  }
}

