import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:zartek_e_commerce/Controller/login_controller.dart';
import 'package:zartek_e_commerce/Utils/custom_textfield.dart';

import '../PageRoute/page_route.dart';
import '../Utils/custom_text.dart';

class LoginView extends StatelessWidget {
  const LoginView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<LoginController>(
        builder: (controller) {
          return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset(
                    'assets/images/firebase_logo.png',
                fit: BoxFit.fill,
                width: 200.w,
                height: 300.h),
                SizedBox(height: 50.h),
                CustomButton(
                  color: Colors.blue,
                  label: 'Google',
                  assetName: 'assets/images/google.png',
                  onTap: () async {
                    var credentials = await controller.signInWithGoogle();
                    if (credentials.user != null) {
                      Get.toNamed(AppRoutes.dashBoard);
                    }
                    print('email= ${credentials.user?.email}');
                    print('displayName= ${credentials.user?.displayName}');
                    print('emailVerified= ${credentials.user?.emailVerified}');
                  },
                ),
                SizedBox(height: 30.h),
                CustomButton(
                  color: Colors.green,
                  label: 'Phone',
                  assetName: 'assets/images/phone.png',
                  onTap: () {
                    Get.bottomSheet(
                      MobileOtpSheet(c: controller),
                    );
                  },
                )
              ]
          );
        }
      ),
    );
  }
}

class CustomButton extends StatelessWidget {
  const CustomButton({super.key, this.onTap, this.color,
    this.label, this.assetName, this.iconEnable = true});

  final Function()? onTap;
  final Color? color;
  final bool iconEnable;
  final String? label;
  final String? assetName;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Center(
        child: Container(
          height: 55.h,
          width: 200.w,
          decoration: BoxDecoration(
              color: color,
              borderRadius: BorderRadius.all(
                  Radius.circular(50.r)
              )
          ),
          child: Row(
            mainAxisAlignment: iconEnable ? MainAxisAlignment.spaceEvenly:
            MainAxisAlignment.center,
            children: [
              iconEnable ?
              Image.asset(
                  assetName??'',
              width: 30.w,height: 30.h):
              const SizedBox(),
              Center(
                child: CustomText(
                  text: label,
                  fontSize: 16.sp,
                  color: Colors.white,
                  fontWeight: FontWeight.w600,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class MobileOtpSheet extends StatelessWidget {
  const MobileOtpSheet({super.key, required this.c});
  final LoginController c;

  @override
  Widget build(BuildContext context) {
    c.otpSent.value = false;
    return Container(
      //height: 600.h,
      width: Get.width,
        padding: EdgeInsets.all(20.h),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.r),
            topRight: Radius.circular(20.r)
          )
        ),
      child: GetX<LoginController>(
        builder: (c) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 20.h),
              CustomText(
                text: 'Enter your mobile number: ',
                fontWeight: FontWeight.w600,
                fontSize: 18.sp,
              ),
              SizedBox(height: 20.h),
              Expanded(
                child: CustomTextField(
                    controller: c.mobileController,
                  isMobile: true,
                ),
              ),
              SizedBox(height: 60.h),
              c.otpSent.value?
              CustomText(
                text: 'Enter OTP sent to your number:',
                fontWeight: FontWeight.w600,
                fontSize: 18.sp,
              ) : const SizedBox(),
              SizedBox(height: 20.h),
              c.otpSent.value?
              Expanded(
                child: SizedBox(
                  width: 200.w,
                  child: CustomTextField(
                      controller: c.otpController,
                  ),
                ),
              ) : const SizedBox(),
              const Spacer(),
              Expanded(
                child: CustomButton(
                  color: Colors.blue,
                  label: 'Submit',
                  iconEnable: false,
                  onTap: () {
                    c.otpSent.value?
                    c.verifyOtp(c.otpController.text) :
                    c.verifyPhoneNumber('+91${c.mobileController.text}');
                  },
                ),
              )
            ],
          );
        }
      ),
    );
  }
}
