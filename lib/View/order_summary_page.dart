import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:zartek_e_commerce/Controller/order_summary_controller.dart';
import 'package:zartek_e_commerce/Utils/custom_text.dart';

import '../Model/get_restaurant_list_model.dart';

class OrderSummaryPage extends StatelessWidget {
  const OrderSummaryPage({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<OrderSummaryController>(
      builder: (c) {
        return Scaffold(
          appBar: AppBar(
            title: CustomText(
              text: 'Order Summary',
              fontSize: 17.sp,
            ),
            backgroundColor: Colors.white,
            elevation: 4,
          ),
          body: Stack(
            children: [
              Card(
                color: Colors.white,
                margin: EdgeInsets.only(
                  left: 20.w,
                  right: 20.w,
                  bottom: 70.h,
                  top: 10.h
                ),
                elevation: 4,
                child: Column(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        color: Colors.green,
                        borderRadius: BorderRadius.all(
                          Radius.circular(8.r)
                        )
                      ),
                      padding: EdgeInsets.symmetric(
                        vertical: 10.h
                      ),
                      margin: EdgeInsets.symmetric(
                        horizontal: 10.w, vertical: 10.h
                      ),
                      child: Center(
                        child: CustomText(
                          text: '${c.dishCount.value} Dishes - ${c.cartCount.value} Items',
                          color: Colors.white,
                          fontSize: 15.sp,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    Expanded(
                      child: GetBuilder<OrderSummaryController>(
                          builder: (c) {
                            print('cartCont= ${c.dataList?[0].categoryDishes?[2].cartCount}');
                            var items = c.dataList;
                            return ListView.builder(
                                itemCount: c.dataList?[0].categoryDishes?.length,
                                itemBuilder: (context, index) =>
                                c.dataList?[0].categoryDishes?[index].cartCount != 0?
                                    DishList(index: index, items: items):
                                const SizedBox());
                          }
                      ),
                    )
                  ],
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: GestureDetector(
                  onTap: () {
                    c.placeOrder();
                  },
                  child: Container(
                    height: 45.h,
                    decoration: BoxDecoration(
                        color: Colors.green,
                        borderRadius: BorderRadius.all(
                            Radius.circular(8.r)
                        )
                    ),
                    padding: EdgeInsets.symmetric(vertical: 10.h),
                    margin: EdgeInsets.symmetric(vertical: 10.h, horizontal: 20.w),
                    child: Center(
                      child: CustomText(
                        text: 'Place order',
                        color: Colors.white,
                        fontSize: 17.sp,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        );
      }
    );
  }
}

class DishList extends StatelessWidget {
  DishList({super.key, required this.items, required this.index});

  List<TableMenuList>? items;
  int index;

  @override
  Widget build(BuildContext context) {
    double totalAmount = (items?[0].categoryDishes?[index].dishPrice??1) * (items?[0].categoryDishes?[index].cartCount??1);
    return Container(
      width: Get.width,
      padding: EdgeInsets.symmetric(
          horizontal: 10.w,
          vertical: 14.h
      ),
      decoration: const BoxDecoration(
          color: Colors.white,
          border: Border(
              bottom: BorderSide(color: Colors.grey),
              top: BorderSide(color: Colors.grey))
      ),
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  decoration: BoxDecoration(
                      border: Border.all(
                          color: Colors.green
                      )
                  ),
                  child: const Icon(Icons.circle, color: Colors.green)),
              SizedBox(width: 10.w),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(
                        width: 100.w,
                        child: CustomText(
                          text: items?[0].categoryDishes?[index].dishName,
                          fontWeight: FontWeight.w600,
                          alignment: TextAlign.start,
                          fontSize: 17.sp,
                        ),
                      ),
                      Container(
                        width: 100.w,
                        height: 30.h,
                        decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius: BorderRadius.all(Radius.circular(10.r))
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            CustomText(
                                text: '-',
                                color: Colors.white,
                                fontSize: 17.sp,
                                fontWeight: FontWeight.w700),
                            CustomText(
                                text: items?[0].categoryDishes?[index].cartCount.toString(),
                                color: Colors.white,
                                fontSize: 17.sp,
                                fontWeight: FontWeight.w700),
                            CustomText(
                                text: '+',
                                color: Colors.white,
                                fontSize: 17.sp,
                                fontWeight: FontWeight.w700),
                          ],
                        ),
                      ),
                      SizedBox(width: 10.w),
                      SizedBox(
                        width: 100.w,
                        child: CustomText(
                          text: '$totalAmount INR',
                          fontWeight: FontWeight.w600,
                          alignment: TextAlign.end,
                          fontSize: 15.sp,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 10.w),
                  CustomText(
                    text: 'INR ${items?[0].categoryDishes?[index].dishPrice}',
                    fontWeight: FontWeight.w600,
                    fontSize: 15.sp,
                  ),
                  SizedBox(height: 5.h),
                  CustomText(
                    text: '${items?[0].categoryDishes?[index].dishCalories} Calories',
                    fontWeight: FontWeight.w600,
                    fontSize: 15.sp,
                  ),
                ],
              )
            ],
          )
        ],
      ),
    );
  }
}
